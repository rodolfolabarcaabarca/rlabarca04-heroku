package root.persistence.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-05-05T13:36:09")
@StaticMetamodel(Ingvehiculos.class)
public class Ingvehiculos_ { 

    public static volatile SingularAttribute<Ingvehiculos, Integer> kms;
    public static volatile SingularAttribute<Ingvehiculos, String> marca;
    public static volatile SingularAttribute<Ingvehiculos, Integer> anho;
    public static volatile SingularAttribute<Ingvehiculos, String> modelo;
    public static volatile SingularAttribute<Ingvehiculos, String> rutDuenho;
    public static volatile SingularAttribute<Ingvehiculos, String> patente;

}