<%-- 
    Document   : index
    Created on : 04-05-2020, 14:41:52
    Author     : rlabarca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>API REST : U3.EV3-Rodolfo Labarca</title>
    </head>
    <body>
        <div class="container">
            <h2>API-REST U3.EV3: Rodolfo Labarca</h2>
            <h3>Documentación:</h3>
            <table class="table table-bordered">
                <thead>
                    <tr class="info">
                        <th>Metodo</th>
                        <th>URL</th>
                        <th>Requisitos</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Listar Vehiculos (GET)</th>
                        <th><a href="/api/vehiculos">/api/vehiculos</a></th>
                        <th>N/A</th>
                    </tr>
                    <tr>
                        <th>Buscar Vehiculo Especifico (GET)</th>
                        <th><a href="/api/vehiculos/{PATENTE}">/api/vehiculos/{PATENTE}</a></th>
                        <th>Debe reemplazar {PATENTE} por la PPU a Buscar por Ej: <a href="/api/vehiculos/KVJB44">/api/vehiculos/KVJB44</a></th>
                    </tr>
                    <tr>
                        <th>Agregar Vehiculo (POST)</th>
                        <th><a href="/api/vehiculos">/api/vehiculos</a></th>
                        <th>Debe enviar un archivo JSON en el siguiente formato: <br>
                            {
                                "anho": 0000,
                                "kms": 0000,
                                "marca": "XXXXXXXX",
                                "modelo": "XXXXXXXX",
                                "patente": "XXXXX",
                                "rutDuenho": "XXXXXXXX-X"
                            }
                        </th>
                    </tr>
                    <tr>
                        <th>Actualizar Registro (PUT)</th>
                        <th><a href="/api/vehiculos">/api/vehiculos</a></th>
                        <th>Debe enviar un archivo JSON en el siguiente formato: <br>
                            {
                                "anho": 0000,
                                "kms": 0000,
                                "marca": "XXXXXXXX",
                                "modelo": "XXXXXXXX",
                                "patente": "XXXXX",
                                "rutDuenho": "XXXXXXXX-X"
                            }
                        </th>
                    </tr>
                    <tr>
                        <th>Eliminar Registro (DELETE)</th>
                        <th><a href="/api/vehiculos/{PATENTE}">/api/vehiculos/{PATENTE}</a></th>
                        <th>Debe reemplazar {PATENTE} por la PPU a Eliminar por Ej: <a href="/api/vehiculos/KVJB44">/api/vehiculos/KVJB44</a></th>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
