/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rlabarca
 */
@Entity
@Table(name = "ingvehiculos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingvehiculos.findAll", query = "SELECT i FROM Ingvehiculos i"),
    @NamedQuery(name = "Ingvehiculos.findByPatente", query = "SELECT i FROM Ingvehiculos i WHERE i.patente = :patente"),
    @NamedQuery(name = "Ingvehiculos.findByMarca", query = "SELECT i FROM Ingvehiculos i WHERE i.marca = :marca"),
    @NamedQuery(name = "Ingvehiculos.findByModelo", query = "SELECT i FROM Ingvehiculos i WHERE i.modelo = :modelo"),
    @NamedQuery(name = "Ingvehiculos.findByKms", query = "SELECT i FROM Ingvehiculos i WHERE i.kms = :kms"),
    @NamedQuery(name = "Ingvehiculos.findByAnho", query = "SELECT i FROM Ingvehiculos i WHERE i.anho = :anho"),
    @NamedQuery(name = "Ingvehiculos.findByRutDuenho", query = "SELECT i FROM Ingvehiculos i WHERE i.rutDuenho = :rutDuenho")})
public class Ingvehiculos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "patente")
    private String patente;
    @Size(max = 25)
    @Column(name = "marca")
    private String marca;
    @Size(max = 25)
    @Column(name = "modelo")
    private String modelo;
    @Column(name = "kms")
    private Integer kms;
    @Column(name = "anho")
    private Integer anho;
    @Size(max = 10)
    @Column(name = "rut_duenho")
    private String rutDuenho;

    public Ingvehiculos() {
    }

    public Ingvehiculos(String patente) {
        this.patente = patente;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getKms() {
        return kms;
    }

    public void setKms(Integer kms) {
        this.kms = kms;
    }

    public Integer getAnho() {
        return anho;
    }

    public void setAnho(Integer anho) {
        this.anho = anho;
    }

    public String getRutDuenho() {
        return rutDuenho;
    }

    public void setRutDuenho(String rutDuenho) {
        this.rutDuenho = rutDuenho;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patente != null ? patente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingvehiculos)) {
            return false;
        }
        Ingvehiculos other = (Ingvehiculos) object;
        if ((this.patente == null && other.patente != null) || (this.patente != null && !this.patente.equals(other.patente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Ingvehiculos[ patente=" + patente + " ]";
    }
    
}
