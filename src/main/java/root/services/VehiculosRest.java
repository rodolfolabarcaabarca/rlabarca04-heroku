/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.model.dao.IngvehiculosDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Ingvehiculos;

/**
 *
 * @author rlabarca
 */
@Path("/vehiculos")
public class VehiculosRest {
    
    /** Objecto DAO **/
    IngvehiculosDAO dao = new IngvehiculosDAO();
   

    /** Listar Todos los Vehiculos **/ 
    @GET 
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarVehiculos(){
        List<Ingvehiculos> getvehi = dao.findIngvehiculosEntities();
        return Response.ok(200).entity(getvehi).build();
    }
    
    /** Buscar Solo un Vehiculo **/ 
    @GET 
    @Path("/{ppuBuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarPPU(@PathParam("ppuBuscar") String ppuBuscar){

        Ingvehiculos vehiculo = dao.findIngvehiculos(ppuBuscar);
        return Response.ok(200).entity(vehiculo).build(); 
    }
    
    /** Metodo para Agregar un Vehiculo **/
    @POST 
    @Consumes(MediaType.APPLICATION_JSON)
        public String addVehiculo(Ingvehiculos nVehiculo) throws Exception{
        String bppu = nVehiculo.getPatente();
        Ingvehiculos cv = dao.findIngvehiculos(bppu);
        String msgSalida = ""; 
        if (cv!=null){
            msgSalida = "** VEHICULO NO INGRESADO **\n PPU: " + nVehiculo.getPatente() + " Ya Existe";
        } else {
            dao.create(nVehiculo);
            msgSalida = "** VEHICULO INGRESADO CORRECTAMENTE **\n PPU: " + nVehiculo.getPatente(); 
        }
        return msgSalida; 
    }
    
    /** Metodo de Actualizar registro **/
    @PUT
    public Response editVehiculo(Ingvehiculos eVehiculo) throws Exception{
        dao.edit(eVehiculo);        
        return Response.ok(eVehiculo).build();
    }
    
    /** Metodo para Eliminar un Registro **/
    @DELETE 
    @Path("/{ppuBorrar}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response delVehiculo(@PathParam("ppuBorrar") String ppuBorrar) throws NonexistentEntityException {
        dao.destroy(ppuBorrar);
        return Response.ok("Vehiculo Eliminado correctamente").build();
    }
    
}
