/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.persistence.entities.Ingvehiculos;

/**
 *
 * @author rlabarca
 */
public class IngvehiculosDAO implements Serializable {

    public IngvehiculosDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public IngvehiculosDAO() {
    }
    
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("vehiculos_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Ingvehiculos ingVehiculo) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(ingVehiculo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findIngvehiculos(ingVehiculo.getPatente()) != null) {
                throw new PreexistingEntityException("Ingvehiculos " + ingVehiculo + " already exists.", ex);                 
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Ingvehiculos ingVehiculo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ingVehiculo = em.merge(ingVehiculo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ingVehiculo.getPatente();
                if (findIngvehiculos(id) == null) {
                    throw new NonexistentEntityException("The ingVehiculo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Ingvehiculos ingVehiculo;
            try {
                ingVehiculo = em.getReference(Ingvehiculos.class, id);
                ingVehiculo.getPatente();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ingVehiculo with id " + id + " no longer exists.", enfe);
            }
            em.remove(ingVehiculo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Ingvehiculos> findIngvehiculosEntities() {
        return findIngvehiculosEntities(true, -1, -1);
    }

    public List<Ingvehiculos> findIngvehiculosEntities(int maxResults, int firstResult) {
        return findIngvehiculosEntities(false, maxResults, firstResult);
    }

    private List<Ingvehiculos> findIngvehiculosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Ingvehiculos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Ingvehiculos findIngvehiculos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Ingvehiculos.class, id);
        } finally {
            em.close();
        }
    }

    public int getIngvehiculosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Ingvehiculos> rt = cq.from(Ingvehiculos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
